<?php

/**
 * @file
 * homepage_feature_stories.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function homepage_feature_stories_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'homepage_feature_stories';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Homepage Feature Stories';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_90fc151ccd7c92c063746eb9440344dd',
      'hide_title' => 1,
      'title' => '1',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta_71b0508a3a151497f03350146527de23',
      'hide_title' => 1,
      'title' => '2',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_8b3f1942d87f0d6dd999e150cb5fc20a',
      'hide_title' => 1,
      'title' => '3',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'views_delta_89774d50160a4f14add07cdc5b296d28',
      'hide_title' => 1,
      'title' => '4',
      'weight' => '-97',
      'type' => 'block',
    ),
    4 => array(
      'bid' => 'views_delta_41da431582bb9965f10c625cb783bd6d',
      'hide_title' => 1,
      'title' => '5',
      'weight' => '-96',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('1');
  t('2');
  t('3');
  t('4');
  t('5');
  t('Homepage Feature Stories');

  $export['homepage_feature_stories'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'homepage_feature_stories_current';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Homepage Feature Stories: Current Students';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_f97a38dba4eb0534e9082c8cbdb7d131',
      'hide_title' => 1,
      'title' => '1',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta_40780e1524cf52cf63acb3ec5c5e3518',
      'hide_title' => 1,
      'title' => '2',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_23d5378b5e359a1c448e5f1674530c64',
      'hide_title' => 1,
      'title' => '3',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'views_delta_d536d4386725fb369a8252bb3a9f8057',
      'hide_title' => 1,
      'title' => '4',
      'weight' => '-97',
      'type' => 'block',
    ),
    4 => array(
      'bid' => 'views_delta_8873a4ab50b3e06cdb2752a865523ffc',
      'hide_title' => 1,
      'title' => '5',
      'weight' => '-96',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('1');
  t('2');
  t('3');
  t('4');
  t('5');
  t('Homepage Feature Stories: Current Students');

  $export['homepage_feature_stories_current'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'homepage_feature_stories_faculty';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Homepage Feature Stories: Faculty';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_f16070dccbc65f63d3c983c6242a7341',
      'hide_title' => 1,
      'title' => '1',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta_d2ab2ee8519b5339b93de0460b11d118',
      'hide_title' => 1,
      'title' => '2',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_c875ade0ffd56150f35f5fc3292eaa77',
      'hide_title' => 1,
      'title' => '3',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'views_delta_27bde44661e4734ce407e963b5e8bb39',
      'hide_title' => 1,
      'title' => '4',
      'weight' => '-97',
      'type' => 'block',
    ),
    4 => array(
      'bid' => 'views_delta_6779a147347ced9a12e5455d8eae348b',
      'hide_title' => 1,
      'title' => '5',
      'weight' => '-96',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('1');
  t('2');
  t('3');
  t('4');
  t('5');
  t('Homepage Feature Stories: Faculty');

  $export['homepage_feature_stories_faculty'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'homepage_feature_stories_future';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Homepage Feature Stories: Future Students';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_eb0d567d73975650dbaf8db1879a04c8',
      'hide_title' => 1,
      'title' => '1',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta_6c1151095244a98f31e17fc6039603e4',
      'hide_title' => 1,
      'title' => '2',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_f8e995cde14712a7a4b32949bdcb9c31',
      'hide_title' => 1,
      'title' => '3',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'views_delta_a46f19e828c6a45814c29a1ef23a0285',
      'hide_title' => 1,
      'title' => '4',
      'weight' => '-97',
      'type' => 'block',
    ),
    4 => array(
      'bid' => 'views_delta_a0a620fca913fc51d99dc68be4d5134f',
      'hide_title' => 1,
      'title' => '5',
      'weight' => '-96',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('1');
  t('2');
  t('3');
  t('4');
  t('5');
  t('Homepage Feature Stories: Future Students');

  $export['homepage_feature_stories_future'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'homepage_feature_stories_staff';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Homepage Feature Stories: Staff';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_b936b183451615b31f39e384f18cffe1',
      'hide_title' => 1,
      'title' => '1',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta_f9718ff8367328fdf81fb7425abf3918',
      'hide_title' => 1,
      'title' => '2',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_f2a730cdfe57f024a5461a4989736b17',
      'hide_title' => 1,
      'title' => '3',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'views_delta_37b5827cbf22024421bc0f3d649652f4',
      'hide_title' => 1,
      'title' => '4',
      'weight' => '-97',
      'type' => 'block',
    ),
    4 => array(
      'bid' => 'views_delta_9c294c1b6a740a14f2b57a847249d456',
      'hide_title' => 1,
      'title' => '5',
      'weight' => '-96',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('1');
  t('2');
  t('3');
  t('4');
  t('5');
  t('Homepage Feature Stories: Staff');

  $export['homepage_feature_stories_staff'] = $quicktabs;

  return $export;
}
